package org.bitbucket.szsiecinski.magnetoclient;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;

public class Klient {
	
	private Socket gniazdoServera;

	public Klient(String adres) throws UnknownHostException, IOException {
		gniazdoServera = new Socket(adres, 9269);
	}
	
	public void przeslijDane(String format) throws IOException {
		DataOutputStream outStream = new DataOutputStream(gniazdoServera.getOutputStream());
		
		outStream.write(format.getBytes(Charset.forName("UTF-8")));
		outStream.close();
	}
	
	public void zakonczSesje() throws IOException {
		gniazdoServera.close();
	}
	
	public boolean jestAktywny() {
		return gniazdoServera.isConnected();
	}

}
