package org.bitbucket.szsiecinski.magnetoclient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.regex.*;

public class MagnetoActivity extends Activity implements SensorEventListener {
	
	private static final String IPADDRESS_PATTERN = 
			"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	private Pattern wzorAdresuIp;
	
	private static Button connect;
	private static EditText inAdresIp;
	private static TextView textConnected;
	
	private SensorManager mSensManager;
	private Sensor mMagnetoSensor;
	float[] popWart = new float[3];
	private static boolean polaczony;
	
	/**
	 * @author uzytkownik
	 * Klasa obsługująca asynchroniczne przesyłanie danych do serwera
	 * na port 9269 TCP/IP.
	 */
	class PrzesylanieDanych extends AsyncTask<String,Void,Void> {

		Klient mKlient;
		
		@Override
		protected Void doInBackground(String... params) {		
			
			try {
				mKlient = new Klient(params[0]);
				
				for(int i=1; i < params.length; i++) {
					mKlient.przeslijDane(params[i]);
				}
				mKlient.zakonczSesje();
				
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
	}
	
	class OnConnectClientListener implements OnClickListener {
		
		Context con;
		
		public OnConnectClientListener(Context con) {
			this.con = con;
		}
		
		@Override
		public void onClick(View v) {
			
			String adresIpString = inAdresIp.getText().toString();
			
			if(connect.getText() == getResources().getString(R.string.connect)) {
				
				Matcher dopasowanie = wzorAdresuIp.matcher(adresIpString);
				
				if(dopasowanie.matches()) {
					connect.setText(R.string.disconnect);
					
					polaczony = true;
					
					textConnected.setText(getResources().getString(R.string.conn_est)
							+ " z serwerem " + adresIpString);
				} else {
					pokazKomunikat("Wpisz prawidłowy adres IP", con);
				}
				
			} else {
				
				polaczony = false;
				
				textConnected.setText(R.string.conn_closed);
				connect.setText(R.string.connect);
			}
		}
	}
	
	private static void pokazKomunikat(String tresc, Context c) {
		AlertDialog.Builder alDlgBuilder = new AlertDialog.Builder(c);
		alDlgBuilder.setMessage(tresc);
		alDlgBuilder.setPositiveButton("OK", null);
		
		alDlgBuilder.show();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_magneto);
		
		inAdresIp = (EditText)this.findViewById(R.id.editText1);
		connect = (Button)this.findViewById(R.id.button1);
		textConnected = (TextView) this.findViewById(R.id.textView2);

		connect.setOnClickListener(new OnConnectClientListener(this));
		
		mSensManager = (SensorManager)this.getSystemService(Context.SENSOR_SERVICE);
		mMagnetoSensor = mSensManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		
		if(mMagnetoSensor==null) {
			pokazKomunikat("Urządzenie nie ma działającego magnetometru", this);
		}
		
		mSensManager.registerListener(this, mMagnetoSensor, SensorManager.SENSOR_DELAY_NORMAL);
		wzorAdresuIp = Pattern.compile(IPADDRESS_PATTERN);
	}
	

	@Override
	public void onSensorChanged(SensorEvent event) {

		String adresIpString = inAdresIp.getText().toString();
		float[] indMag = event.values;

		String formatDanych = String
				.format(new Locale("pl_PL"),
						"Indukcja magnetyczna X: %g µT\nIndukcja magnetyczna Y: %g µT\nIndukcja magnetyczna Z: %g µT",
						indMag[0], indMag[1], indMag[2]);

		if (event.accuracy != SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM) {
			formatDanych = String
					.format(new Locale("pl_PL"),
							"Indukcja magnetyczna X: %g µT\nIndukcja magnetyczna Y: %g µT\nIndukcja magnetyczna Z: %g µT",
							indMag[0], indMag[1], indMag[2]);

			if (polaczony) {
				new PrzesylanieDanych().execute(adresIpString, formatDanych);
			}

			popWart = indMag;
		}

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
	
	
}
